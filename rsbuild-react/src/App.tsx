import React, { useState } from 'react'

import SchemaBuilder from '@xrenders/schema-builder';
import ReactDOM from 'react-dom';
window.React = React;
window.ReactDOM = ReactDOM;


export default function App() {
  return <div style={{ height: '80vh' }}>
    <SchemaBuilder />
  </div>
}
