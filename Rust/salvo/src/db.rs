mod db;

// use crate::config::CFG;
// use sqlx::SqlitePool;
use sqlx::MySqlPool;
use tokio::sync::OnceCell;

//pub static DB: OnceCell<SqlitePool> = OnceCell::const_new();
pub static DB: OnceCell<MySqlPool> = OnceCell::const_new();

pub async fn init_db_conn() {
    DB.get_or_init(|| async {
        // SqlitePool::connect("sqlite:./demo.db")
        MySqlPool::connect("mysql://root:rootroot@localhost/test").await.expect("Database connection failed.")
    }).await;
}
