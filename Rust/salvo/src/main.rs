use salvo::prelude::*;
use serde::{Deserialize, Serialize};
use sqlx::prelude::FromRow;

#[handler]
fn hello() -> String {
    String::from("Hello World")
}

#[handler]
async fn show_writer(req: &mut Request) -> String {
    let id = req.query::<String>("id").unwrap();
    println!("id: {}", id);
    id
}


#[derive(Deserialize, Serialize, Extractible, Eq, PartialEq, Debug)]
#[salvo(extract(default_source(from = "body", parse = "json")))]
struct MyData {
    a: String,
    b: i32,
}

#[handler]
async fn t1(req: &mut Request, res: &mut Response) {
    let data = req.parse_json::<MyData>().await.unwrap();
    println!(" data: {:?}", data);
    // "ok".to_string()
    res.render(Json(data));
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt().init();
    // db::init_db_conn().await;
    let router = Router::new()
        .get(hello)
        .push(Router::with_path("api").get(show_writer).post(t1));
    let acceptor = TcpListener::new("127.0.0.1:5800").bind().await;
    Server::new(acceptor).serve(router).await;
}



#[derive(FromRow, Serialize, Debug)]
pub struct User {
    pub id: i64,
    pub username: String,
    pub password: String,
}

