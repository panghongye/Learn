import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  css: {
    preprocessorOptions: {
      less: {
        // 传递给 less 的选项
        // 例如，可以在这里定义全局的 Less 变量
        // modifyVars: {
        //   'primary-color': '#1DA57A',
        //   'link-color': '#1DA57A',
        //   'border-radius-base': '2px',
        // },
        // 或者添加 Less 插件
        // plugins: [new LessPluginCleanCSS()],
        // javascriptEnabled: true, // 如果你的 Less 文件中使用了 JavaScript
      },
    },
  }
})
