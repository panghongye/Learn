import React from 'react';
// import SchemaBuilder from '@xrenders/schema-builder';
import SchemaBuilder from './schema-builder';
import ReactDOM from 'react-dom';


// global.React = React;
// global.ReactDOM = ReactDOM;




const Demo = () => {
 return <div style={{ height: '80vh' }}>
   <SchemaBuilder importBtn={true} exportBtn={true} pubBtn={false} />
 </div>;
};

export default Demo;