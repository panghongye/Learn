import React, { useState } from 'react';
import { Input, Button, Checkbox, InputNumber, Radio, DatePicker, Select, Upload, Form } from '@ones-design/core';
import { CopyOutlined, DeleteOutlined, DragOutlined } from '@ant-design/icons';
import './FormRender.css'
// import UserSel from './UserSel';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


export interface Option {
  // [key: string]: any;
  type: 'Input' | 'TextArea' | 'InputNumber' | 'File' | 'Radio' | 'Checkbox' | 'Select:Single' | 'Select:Multiple' | 'DatePicker' | 'Explain' | 'Member' | 'Project' | 'WorkItem' | 'PageGroup' | 'WikiPage'
  name: string
  required?: boolean | undefined
  placeholder?: string
  precision?: number,
  options?: string[]
  props?: any
  value?: any //type =Explain 时的值
  maxLength?: number //type =Member 时 指定最大选择数量
}

export interface IProps {
  pattern: '设计' | '使用'
  options: Option[],
  optionsSet: (value: Option[]) => void
  onchange?: (value: Option[]) => void
  title?: string
  form?: any // Form.useForm()
}

export default function FormRender(props: IProps) {
  const { pattern, options, onchange, optionsSet, form } = props
  const [selectedOption, selectedOptionSet] = useState(options[0] || {})
  const [selectedOptionIndex, selectedOptionIndexSet] = useState(0)
  function onValueChange() {
    selectedOptionSet(JSON.parse(JSON.stringify(selectedOption)))
    optionsSet(JSON.parse(JSON.stringify(options)))
    onchange && onchange(JSON.parse(JSON.stringify(options)))
  }

  function onOptionSetChange(k: string, v: any) {
    selectedOption[k] = v
    options[selectedOptionIndex] = selectedOption
    selectedOptionSet(selectedOption)
    onValueChange()
  }

  function optionsPush(type: string) {
    const option = getOptionByType(type)
    options.push(option)
    selectedOptionSet(option)
    selectedOptionIndexSet(options.length - 1)
    onValueChange()
  }


  function getOptionByType(type: string,) {
    let option: any
    if (type == 'Input') option = ({ type, name: '单行输入框', required: true, placeholder: '请输入' })
    if (type == 'TextArea') option = ({ type, name: '多行输入框', required: true, placeholder: '请输入' })
    if (type == 'InputNumber') option = ({ type: 'InputNumber', name: '数字输入框', required: true, placeholder: '请输入数字', precision: 0 })
    if (type == 'File') option = ({ type, name: '附件', required: true, })
    if (type == 'Radio') option = ({ type, name: '单选', required: true, options: ['选项1', '选项2'] })
    if (type == 'Checkbox') option = ({ type, name: '多选', required: true, options: ['选项1', '选项2'] })
    if (type == 'Select:Single') option = ({ type, name: '下拉单选', required: true, options: ['选项1', '选项2'] })
    if (type == 'Select:Multiple') option = ({ type, name: '下拉多选', required: true, options: ['选项1', '选项2'] })
    if (type == 'DatePicker') option = ({ type, name: '日期时间', required: true, props: { showTime: true } })
    if (type == 'Explain') option = ({ type, name: '说明文字', placeholder: '请输入文字说明', value: '说明文字' })
    // ONES
    if (type == 'Member') option = ({ type, name: '成员', required: true, placeholder: '请选择', maxLength: 1 })
    if (type == 'Project') option = ({ type, name: '项目', required: true, })
    if (type == 'WorkItem') option = ({ type, name: '工作项', required: true, })
    if (type == 'PageGroup') option = ({ type, name: '页面组', required: true, })
    if (type == 'WikiPage') option = ({ type, name: 'Wiki页面', required: true, })
    return option
  }

  const onDragEnd = (result: any) => {
    const { source, destination } = result;
    if (!destination) return;
    if (destination.droppableId == 'formControl') return
    // 排序
    if (source.droppableId == destination.droppableId && source.droppableId == 'optionsList') {
      const newItems = Array.from(options);
      const [reorderedItem] = newItems.splice(result.source.index, 1);
      newItems.splice(result.destination.index, 0, reorderedItem);
      optionsSet(newItems);
    }
    // add
    if (source.droppableId == 'formControl' && destination.droppableId == 'optionsList') {
      const option = getOptionByType(result.draggableId)
      const newItems = Array.from(options);
      newItems.splice(result.destination.index, 0, option);
      optionsSet(newItems);
      onValueChange
    }
  };

  const baseControls = [
    { label: '单行输入框', type: 'Input' },
    { label: '多行输入框', type: 'TextArea' },
    { label: '数字输入框', type: 'InputNumber' },
    { label: '附件', type: 'File' },
    // { label: '单选', type: 'Radio' },
    // { label: '多选', type: 'Checkbox' },
    { label: '下拉单选', type: 'Select:Single' },
    { label: '下拉多选', type: 'Select:Multiple' },
    { label: '日期时间', type: 'DatePicker' },
    { label: '说明文字', type: 'Explain' },
  ]

  const onesControls = [
    { label: '成员', type: 'Member' },
    { label: '项目', type: 'Project' },
    { label: '工作项', type: 'WorkItem' },
    { label: '页面组', type: 'PageGroup' },
    { label: 'wiki页面', type: 'WikiPage' },
  ]

  return (
    <>
      <DragDropContext onDragEnd={onDragEnd} >
        <div className='form-container'>
          <Droppable droppableId="formControl">
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                style={{
                  padding: 8,
                  minHeight: '200px',
                  border: '1px solid #ccc',
                  background: snapshot.isDraggingOver ? '#f3f3f3' : 'white'
                }}
              >
                {/* 表单菜单 */}
                {pattern == '设计' && <div className='form-container-menus'>
                  <h3>基础控件</h3>
                  <div className='form-container-menu' >
                    {baseControls.map((a, b) =>
                      <Draggable
                        key={b}
                        draggableId={a.type}
                        index={b}
                      >
                        {(provided, snapshot) => (
                          <div key={b} className='form-container-menu-btn' onClick={() => optionsPush(a.type)}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={{
                              userSelect: 'none',
                              border: '1px solid',
                              borderRadius: 4,
                              padding: 1,
                              borderColor: snapshot.isDragging ? 'rgb(0, 100, 255)' : '#ccc',
                              color: '#333',
                              ...provided.draggableProps.style,
                              textAlign: 'center'
                            }}

                          > {a.label}
                          </div>
                        )}
                      </Draggable>
                    )}
                  </div>

                  <h3 style={{ marginTop: '20px' }}>ONES控件</h3>
                  <div className='form-container-menu'>
                    {onesControls.map((a, b) =>
                      <Draggable key={b} draggableId={a.type} index={b}>
                        {(provided, snapshot) => (
                          <div key={b} className='form-container-menu-btn' onClick={() => optionsPush(a.type)}
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            style={{
                              userSelect: 'none',
                              border: '1px solid',
                              borderRadius: 4,
                              padding: 1,
                              borderColor: snapshot.isDragging ? 'rgb(0, 100, 255)' : '#ccc',
                              color: '#333',
                              ...provided.draggableProps.style,
                              textAlign: 'center'
                            }}

                          > {a.label}
                          </div>
                        )}
                      </Draggable>
                    )}
                  </div>
                </div>}
              </div>
            )}
          </Droppable>
          {/* 表单设计 */}
          <div className='form-container-form-div'>
            <div className='form-container-form'>
              <div className='form-container-form-title'>{props?.title}</div>
              <Form layout='vertical' form={form} name='formControls' className='formControls'>
                <Droppable droppableId="optionsList">
                  {(provided) => (
                    <div {...provided.droppableProps} ref={provided.innerRef} className='optionsList'>
                      {
                        options.map((item, index) => {
                          return (
                            <Draggable key={item.name + index} draggableId={item.name + index} index={index}>
                              {(provided) => (
                                <div
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  ref={provided.innerRef}
                                >
                                  <div className='FormItemDiv' onClick={(e) => {
                                    selectedOptionSet(item)
                                    selectedOptionIndexSet(index)
                                  }}>
                                    {selectedOptionIndex == index && <div style={{ position: 'absolute', left: 0, width: 4, height: '100%', background: '#0064FF' }} />}
                                    <div className='action-btns'>
                                      <CopyOutlined className='btn' onClick={() => {
                                        let v = JSON.parse(JSON.stringify(item))
                                        v.name += '副本'
                                        options.splice(index + 1, 0, v);
                                        onValueChange()
                                      }} />|
                                      <DeleteOutlined className='btn' onClick={() => { options.splice(index, 1), onValueChange() }} />
                                    </div>
                                    <Form.Item className='FormItem' name={item.name} rules={[{ required: item.required }]} label={item.name}>
                                      {item.type == 'Input' && <Input placeholder={item.placeholder || '请输入'} crossOrigin='anonymous' />}
                                      {item.type == 'TextArea' && <Input.TextArea placeholder={item.placeholder || '请输入'} rows={3} />}
                                      {item.type == 'InputNumber' && <InputNumber placeholder={item.placeholder || '请输入'} precision={item.precision} crossOrigin={undefined} />}
                                      {item.type == 'File' && < Upload.Dragger />}
                                      {item.type == 'Select:Single' && <Select options={item.options?.map((a) => ({ label: a, value: a }))} placeholder={item.placeholder || '请选择'} />}
                                      {item.type == 'Select:Multiple' && <Select mode='multiple' options={item.options?.map((a) => ({ label: a, value: a }))} placeholder={item.placeholder || '请选择'} />}
                                      {item.type == 'Radio' && <Radio.Group options={item.options} />}
                                      {item.type == 'Checkbox' && <Checkbox.Group options={item.options} />}
                                      {item.type == 'DatePicker' && <DatePicker {...item.props} placeholder={item.placeholder || '请选择'} />}
                                      {item.type == 'Explain' && <Input placeholder={item.placeholder || '请输入'} readOnly value={item.options} crossOrigin={undefined} />}
                                      {/* ONES */}
                                      {item.type == 'Member' && <UserSel placeholder={item.placeholder || '请选择'} />}
                                      {item.type == 'Project' && <Select placeholder={item.placeholder || '请选择'} />}
                                      {item.type == 'WorkItem' && <Input placeholder={item.placeholder || '输入工作项标题或 ID 搜索'} crossOrigin={undefined} />}
                                      {item.type == 'PageGroup' && <Select placeholder={item.placeholder || '请选择'} />}
                                      {item.type == 'WikiPage' && <Select placeholder={item.placeholder || '请选择'} />}
                                    </Form.Item>
                                  </div>
                                </div>
                              )}
                            </Draggable>
                          )
                        })
                      }
                    </div>
                  )}
                </Droppable>
                {pattern == '设计' && <div className='formControlsTips'><DragOutlined style={{ marginRight: 10 }} />点击或拖拽左侧控件到此处</div>}
              </Form>
            </div>
          </div>
          {/* 属性设置 */}
          {pattern == '设计' && <div className='form-container-setting'>
            <h3>{selectedOption.type}</h3>
            <Form layout='vertical'>
              {(selectedOption.type == 'Input'
                || selectedOption.type == 'TextArea'
                || selectedOption.type == 'InputNumber'
                || selectedOption.type == 'File'
                || selectedOption.type == 'Radio'
                || selectedOption.type == 'Checkbox'
                || selectedOption.type == 'Select:Single'
                || selectedOption.type == 'Select:Multiple'
                || selectedOption.type == 'DatePicker'
                || selectedOption.type == 'Explain'
                || selectedOption.type == 'Member'
                || selectedOption.type == 'Project'
                || selectedOption.type == 'WorkItem'
                || selectedOption.type == 'PageGroup'
                || selectedOption.type == 'WikiPage'
              ) && <Form.Item label='标题' rules={[{ required: true }]} >
                  <Input value={selectedOption?.name} maxLength={64} onChange={(e) => { onOptionSetChange('name', e.target.value); }} placeholder='请输入' crossOrigin={undefined} />
                </Form.Item>}

              {(selectedOption.type == 'Input'
                || selectedOption.type == 'TextArea'
                || selectedOption.type == 'InputNumber'
                || selectedOption.type == 'Radio'
                || selectedOption.type == 'Checkbox'
                || selectedOption.type == 'Select:Single'
                || selectedOption.type == 'Select:Multiple'
                || selectedOption.type == 'DatePicker'
                || selectedOption.type == 'Member'
                || selectedOption.type == 'Project'
                || selectedOption.type == 'WorkItem'
                || selectedOption.type == 'PageGroup'
                || selectedOption.type == 'WikiPage'
              ) && <Form.Item label='默认提示'  >
                  <Input value={selectedOption.placeholder} onChange={(e) => { onOptionSetChange('placeholder', e.target.value); }} placeholder='请输入' crossOrigin={undefined} />
                </Form.Item>}

              {(selectedOption.type == 'InputNumber') && <Form.Item label='小数位数' rules={[{ required: true }]}>
                <InputNumber precision={0} onChange={(e) => { onOptionSetChange('precision', e); }} min={0} placeholder='不限制' crossOrigin={undefined} />
              </Form.Item>}

              {(selectedOption.type == 'Radio' || selectedOption.type == 'Checkbox' || selectedOption.type == 'Select:Single' || selectedOption.type == 'Select:Multiple') && <Form.Item label='选项' >
                {selectedOption.options?.map((a, b: any) => {
                  return <Form.Item key={b}> <Input value={a} placeholder='请输入' style={{ width: '70%' }} onChange={(e) => {
                    let o = selectedOption?.options || [];
                    o[b] = e.target.value;
                    options[selectedOptionIndex] = selectedOption;
                    onValueChange();
                  }} crossOrigin={undefined} /> <DeleteOutlined onClick={() => {
                    selectedOption?.options?.splice(b, 1)
                    onValueChange()
                  }} /> </Form.Item>
                })}
                {selectedOption?.options?.length && selectedOption?.options?.length < 10 &&
                  <Button type='link' style={{ display: 'block' }} onClick={() => {
                    let o = selectedOption.options || []
                    o.push('')
                    onValueChange()
                  }}>+新增选项</Button>
                }
              </Form.Item>}

              {(selectedOption.type == 'Input'
                || selectedOption.type == 'TextArea'
                || selectedOption.type == 'InputNumber'
                || selectedOption.type == 'File'
                || selectedOption.type == 'Radio'
                || selectedOption.type == 'Checkbox'
                || selectedOption.type == 'Select:Single'
                || selectedOption.type == 'Select:Multiple'
                || selectedOption.type == 'DatePicker'
                || selectedOption.type == 'Member'
                || selectedOption.type == 'Project'
                || selectedOption.type == 'WorkItem'
                || selectedOption.type == 'PageGroup'
                || selectedOption.type == 'WikiPage'
              ) && <Form.Item label='其他' >
                  <Checkbox checked={selectedOption.required} onChange={(e) => { onOptionSetChange('required', e.target.checked) }} /> 必填
                </Form.Item>}

              {(selectedOption.type == 'DatePicker') && <Form.Item label='日期格式' >
                <Select options={[{ label: '年-月-日', value: false as any }, { label: '年-月-日 时:分:秒', value: true as any }]} placeholder='请选择'
                  value={selectedOption.props.showTime} onChange={(e) => { onOptionSetChange('props', { ...selectedOption.props, showTime: e }) }} />
              </Form.Item>}

              {(selectedOption.type == 'Explain') && <Form.Item label='说明文字' >
                <Input.TextArea value={selectedOption.value} maxLength={1000} placeholder='请输入说明文字' rows={3} onChange={(e) => { onOptionSetChange('value', e.target.value) }} />
              </Form.Item>}

              {(selectedOption.type == 'Member') && <Form.Item label='选择方式' >
                <Radio.Group layout='vertical' value={selectedOption.maxLength && selectedOption.maxLength > 1 ? '多选' : '单选'} onChange={v => {
                  if (v.target.value == '多选') onOptionSetChange('maxLength', 2)
                  else onOptionSetChange('maxLength', 1)
                }}>
                  <Radio value='单选' >单选</Radio>
                  <Radio value='多选'  >多选，最大可选择数量<InputNumber value={selectedOption.maxLength} min={1} precision={0} style={{ marginLeft: '10px', width: '100px' }} onChange={(e) => {
                    onOptionSetChange('maxLength', e)
                  }} crossOrigin={undefined} /></Radio>
                </Radio.Group>
              </Form.Item>}

            </Form>
          </div>}
        </div >
      </DragDropContext>
    </>
  );
};
