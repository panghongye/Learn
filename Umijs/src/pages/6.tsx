import React, { useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const initialData = {
  columns: {
    columnA: {
      id: 'columnA',
      title: 'Column A',
      taskIds: ['task1', 'task2', 'task3']
    },
    columnB: {
      id: 'columnB',
      title: 'Column B',
      taskIds: ['task4', 'task5']
    }
  },
  tasks: {
    task1: { id: 'task1', content: 'Task 1' },
    task2: { id: 'task2', content: 'Task 2' },
    task3: { id: 'task3', content: 'Task 3' },
    task4: { id: 'task4', content: 'Task 4' },
    task5: { id: 'task5', content: 'Task 5' }
  },
  columnOrder: ['columnA', 'columnB']
};

const App = () => {
  const [data, setData] = useState(initialData);

/**
 * Handles drag and drop events after completion.
 * @param result The result object of the drag and drop operation, containing information about the destination, source, and draggable element.
 */
const onDragEnd = (result) => {
  // 从结果中解构获取目标、源和 draggableId 信息
  const { destination, source, draggableId } = result;
  
  //如果没有目的地信息，说明拖动操作被取消，所以直接返回
  if (!destination) return;
  
  // 如果目标和源在同一个列表中，并且放置位置没有改变，则不执行任何操作
  if (destination.droppableId === source.droppableId && destination.index === source.index) return;

  // 获取源和目标列表
  const start = data.columns[source.droppableId];
  const finish = data.columns[destination.droppableId];

  // I如果源列表和目标列表相同，则处理列表中的重新排序
  if (start === finish) {
    //为源列表创建一个新的 taskIds 数组，删除源索引处的任务，并将其插入到目标索引处
    const newTaskIds = Array.from(start.taskIds);
    newTaskIds.splice(source.index, 1);
    newTaskIds.splice(destination.index, 0, draggableId);

    // 使用新的 taskIds 更新源列表
    const newColumn = {
      ...start,
      taskIds: newTaskIds
    };

    //使用新的源列表更新数据
    const newData = {
      ...data,
      columns: {
        ...data.columns,
        [newColumn.id]: newColumn
      }
    };

    setData(newData);
    return;
  }

  // 如果源列表和目标列表不同，则处理列表之间的移动 
  // 从一个列表移动到另一个列表
  const startTaskIds = Array.from(start.taskIds);
  startTaskIds.splice(source.index, 1);
  const newStart = { ...start, taskIds: startTaskIds };

  const finishTaskIds = Array.from(finish.taskIds);
  finishTaskIds.splice(destination.index, 0, draggableId);
  const newFinish = { ...finish, taskIds: finishTaskIds };

  // 使用新的源和目标列表更新数据
  const newData = {
    ...data,
    columns: { ...data.columns, [newStart.id]: newStart, [newFinish.id]: newFinish }
  };

  setData(newData);
};

  return (
    <DragDropContext onDragEnd={onDragEnd}>

      <div style={{ display: 'flex', }}>
        {data.columnOrder.map((columnId) => {
          const column = data.columns[columnId];
          const tasks = column.taskIds.map((taskId) => data.tasks[taskId]);

          return (
            <Droppable droppableId={column.id} key={column.id}>
              {(provided) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={{
                    width: '200px',
                    margin: '10px',
                    border: '1px solid black',
                    padding: '10px'
                  }}
                >
                  <h3>{column.title}</h3>
                  {tasks.map((task, index) => (
                    <Draggable key={task.id} draggableId={task.id} index={index}>
                      {(provided) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={{
                            userSelect: 'none',
                            padding: '16px',
                            margin: '0 0 8px 0',
                            minHeight: '50px',
                            backgroundColor: 'lightblue',
                            ...provided.draggableProps.style
                          }}
                        >
                          {task.content}
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          );
        })}
      </div>
    </DragDropContext>
  );
};

export default App;
