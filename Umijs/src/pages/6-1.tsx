import React, { useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

function App() {
  const [columns, setColumns] = useState({
    column1: ['item1', 'item2', 'item3'],
    column2: ['item4', 'item5', 'item6']
  });

  // 该函数是用于处理拖拽结束后的逻辑。根据拖拽结果，它可以在同一列中重新排序元素，或者在两列之间移动元素。具体处理方式如下：
  // 首先判断拖拽结果中是否有目标位置，如果没有（即拖拽到列表外），则直接返回，不做任何处理。
  // 如果拖拽元素所在的列和目标位置所在的列相同，则调用reorder函数重新排序该列的元素，并使用setColumns更新状态。
  // 如果拖拽元素所在的列和目标位置所在的列不同，则分别获取两列的元素数组，从源列中移除拖拽元素，并将其插入到目标列中的目标位置。然后使用setColumns更新状态。
  // 该函数通过判断拖拽元素的来源和目标位置，实现了对元素的重新排序和移动操作，并通过状态更新来反映拖拽后的结果。

  const onDragEnd = (result: { destination: any; source?: any; }) => {
    //  掉在名单之外
    if (!result.destination) return
    const { source, destination } = result;
    // 在同一列内重新排序
    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        columns[source.droppableId],
        source.index,
        destination.index
      );
      setColumns(prevState => ({
        ...prevState,
        [source.droppableId]: items
      }));
    } else {
      // 在两列之间移动
      const sourceItems = columns[source.droppableId];
      const destItems = columns[destination.droppableId];
      const [removed] = sourceItems.splice(source.index, 1);

      destItems.splice(destination.index, 0, removed);

      setColumns({
        ...columns,
        [source.droppableId]: sourceItems,
        [destination.droppableId]: destItems
      });
    }
  };

  // 该函数用于重新排列数组中的元素
  function reorder(list: Iterable<unknown> | ArrayLike<unknown>, startIndex: number, endIndex: number) {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <div style={{ display: 'flex', gap: '1em' }}>
        <Droppable droppableId="column1">
          {(provided, snapshot) => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{
                padding: 8,
                width: '300px',
                minHeight: '200px',
                border: '1px solid #ccc',
                background: snapshot.isDraggingOver ? '#f3f3f3' : 'white'
              }}
            >
              
              {columns.column1.map((item, index) => (
                <Draggable
                  key={item}
                  draggableId={item}
                  index={index}
                >
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={{
                        userSelect: 'none',
                        outline: '1px solid #ccc',
                        padding: 16,
                        margin: `0 0 ${index === columns.column1.length - 1 ? 0 : 8}px 0`,
                        minHeight: '50px',
                        backgroundColor: snapshot.isDragging ? '#f9c' : '#fff',
                        color: '#333',
                        ...provided.draggableProps.style
                      }}
                    >
                     
                      
                      
                      {item}



                    </div>
                  )}
                </Draggable>
              ))}


            </div>
          )}
        </Droppable>

        <Droppable droppableId="column2">
          {(provided, snapshot) => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{
                padding: 8,
                width: '300px',
                minHeight: '200px',
                border: '1px solid #ccc',
                background: snapshot.isDraggingOver ? '#f3f3f3' : 'white'
              }}
            >
              {columns.column2.map((item, index) => (
                <Draggable
                  key={item}
                  draggableId={item}
                  index={index}
                >
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={{
                        outline: '1px dashed #ccc',
                        userSelect: 'none',
                        padding: 16,
                        margin: `0 0 ${index === columns.column2.length - 1 ? 0 : 8}px 0`,
                        minHeight: '50px',
                        backgroundColor: snapshot.isDragging ? '#f9c' : '#fff',
                        color: '#333',
                        ...provided.draggableProps.style
                      }}
                    >
                      {item}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </div>
    </DragDropContext>
  );
}

export default App;