import React from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


function App() {
  const [items, setItems] = React.useState([
    { id: '1', content: 'Item 1' },
    { id: '2', content: 'Item 2' },
    { id: '3', content: 'Item 3' },
  ])

  const onDragEnd = (result) => {
    if (!result.destination) return
    const newItems = Array.from(items);
    const [reorderedItem] = newItems.splice(result.source.index, 1);
    newItems.splice(result.destination.index, 0, reorderedItem);
    setItems(newItems);
  };




  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable_>
        {items.map((item, index) => (
          <Draggable_ key={item.id} id={item.id} index={index}>
            {item.content}
          </Draggable_>
        ))}
      </Droppable_>
    </DragDropContext>
  );
}

export default App;


export function Droppable_(props) {
  return <Droppable droppableId="items">
    {(provided) => (
      <div {...provided.droppableProps} ref={provided.innerRef}>
        {props.children}
      </div>
    )}
  </Droppable>
}

export function Draggable_(props) {
  return <Draggable draggableId={props.id} index={props.index}>
    {(provided) => (
      <div {...provided.draggableProps} {...provided.dragHandleProps} ref={provided.innerRef}>
        {props.children}
      </div>
    )}
  </Draggable>
}