import React, { useState } from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useDrag, useDrop, DndProvider } from 'react-dnd';
import { Button } from 'antd';

export const Item = ({ id, index, moveItem, children ,className}) => {
  const [, ref] = useDrag({
    type: 'ITEM',
    item: { id, index },
  });

  const [, drop] = useDrop({
    accept: 'ITEM',
    drop(item: any) {
      if (item.index !== index) {
        moveItem(item.index, index);
        item.index = index;
      }
    },
    hover(item, monitor){},
  });

  return (
    <div>
      <div ref={(node) => ref(drop(node))}  className={className} >
        {children}
      </div>
    </div>
  );
};




export default function App() {
  const [items, setItems] = useState([
    { id: 1, text: 'Item 1' },
    { id: 2, text: 'Item 2' },
    { id: 3, text: 'Item 3' },
    { id: 4, text: 'Item 4' },
  ]);

  const moveItem = (fromIndex, toIndex) => {
    const updatedItems = [...items];
    const [movedItem] = updatedItems.splice(fromIndex, 1);
    updatedItems.splice(toIndex, 0, movedItem);
    setItems(updatedItems);
  };


  return (
    <DndProvider backend={HTML5Backend}>
      {items.map((item, index) => (
        <Item
          id={item}
          key={JSON.stringify(item)}
          index={index}
          moveItem={moveItem}
        >
          <Button>{item.text}</Button>
        </Item>
      ))}
    </DndProvider>
  );
}