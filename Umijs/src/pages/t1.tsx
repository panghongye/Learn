import React, { useState } from 'react';
import { Table } from '@ones-design/table';

// 假设simpleColumns和originDataSource是外部提供的数据
export const simpleColumns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age'
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address'
  }
];
// 示例数据
export const originDataSource = [
  {
    key: 1,
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    draggable: true,
    date2: null
  },
  {
    key: 2,
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    draggable: true,
    date2: null
  },
  {
    key: 3,
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    draggable: true,
    date2: null
  },
  {
    key: 4,
    name: 'Jane Doe',
    age: 28,
    address: 'Sydney No. 2 Lake Park',
    draggable: true,
    date2: null
  }
];

const DraggableList = (props) => {
  const { draggableArgs = {}, dataSource, setDataSource, columns } = props
  const insertToIndex = React.useCallback((arr, index, dropIndex) => {
    const tempArr = [...arr];
    tempArr[index] = undefined;
    if (dropIndex > index) {
      tempArr.splice(dropIndex + 1, 0, arr[index]);
    } else {
      tempArr.splice(dropIndex, 0, arr[index]);
    }
    return tempArr.filter(Boolean);
  }, []);

  const onDragStart = React.useCallback(arg => {
    // console.log('测试 onDragStart 参数: ', arg);
  }, []);

  const onDrop = React.useCallback(info => {
    // console.log('测试 onDrop 参数: ', info);
    const { dragNode, node } = info;
    const dropIndex = Number(node.pos);
    const dragIndex = Number(dragNode.pos);
    setDataSource(preDataSource => insertToIndex(preDataSource, dragIndex, dropIndex));
  }, [insertToIndex]);

  const checkDraggable = React.useCallback(record => {
    return typeof record.draggable === 'undefined' ? true : record.draggable;
  }, []);

  const nodeDraggable = React.useCallback((record, rowIndex) => {
    return checkDraggable(record);
  }, [checkDraggable]);

  const draggableRender = React.useCallback((record, index, originNode) => {
    return checkDraggable(record) ? originNode : null;
  }, [checkDraggable]);

  const draggable = React.useMemo(() => ({
    ...draggableArgs,
    onDragStart,
    onDrop,
    nodeDraggable,
    render: draggableRender
  }), [onDragStart, onDrop, nodeDraggable, draggableRender, draggableArgs]);

  return (
    <Table
      // height={300}
      {...props}
      draggable={draggable}
      columns={columns}
      dataSource={dataSource}
    />
  );
};

export default () => {
  const [dataSource, setDataSource] = useState(originDataSource)
  return <DraggableList dataSource={dataSource} columns={simpleColumns} setDataSource={setDataSource} />
}