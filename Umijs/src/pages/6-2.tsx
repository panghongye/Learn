import FormRender from "@/C/FormRender"
import { useState } from "react"


export default function () {
  const [options, optionssSet] = useState([
    {
      type: 'Input',
      name: '单行输入框',
      placeholder: 'placeholder',
      required: true,
    },
    {
      type: 'TextArea',
      name: '多行输入框',
      placeholder: 'placeholder',
      required: true,
    },
  ]) as any
  return <FormRender pattern='设计' options={options} optionsSet={optionssSet} />
}