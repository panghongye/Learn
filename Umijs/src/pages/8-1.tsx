import React, { useState } from 'react';

export default function DragAndDrop() {
  const [columns, setColumns] = useState({
    column1: ['Item 1', 'Item 2', 'Item 3'],
    column2: ['Item 4', 'Item 5', 'Item 6']
  });

  const handleDragStart = (e, item, column) => {
    e.dataTransfer.setData('text/plain', JSON.stringify({ item, column }));
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

  const handleDrop = (e, targetColumn) => {
    e.preventDefault();
    const data = JSON.parse(e.dataTransfer.getData('text/plain'));
    const { item, column: sourceColumn } = data;

    if (sourceColumn !== targetColumn) {
      const newColumns = { ...columns };
      newColumns[sourceColumn] = newColumns[sourceColumn].filter(i => i !== item);
      newColumns[targetColumn] = [...newColumns[targetColumn], item];
      setColumns(newColumns);
    }
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'space-around', width: 600 }}>
      <div
        onDragOver={handleDragOver}
        onDrop={(e) => handleDrop(e, 'column1')}
        style={{ border: '1px solid black', padding: '10px', width: '200px' }}
      >
        <h2>Column 1</h2>
        {columns.column1.map((item, index) => (
          <div
            key={index}
            draggable
            onDragStart={(e) => handleDragStart(e, item, 'column1')}
            style={{ border: '1px solid gray', padding: '5px', margin: '5px', backgroundColor: 'white' }}
          >
            {item}
          </div>
        ))}
      </div>
      <div
        onDragOver={handleDragOver}
        onDrop={(e) => handleDrop(e, 'column2')}
        style={{ border: '1px solid black', padding: '10px', width: '200px' }}
      >
        <h2>Column 2</h2>
        {columns.column2.map((item, index) => (
          <div
            key={index}
            draggable
            onDragStart={(e) => handleDragStart(e, item, 'column2')}
            style={{ border: '1px solid gray', padding: '5px', margin: '5px', backgroundColor: 'white' }}
          >
            {item}
          </div>
        ))}
      </div>
    </div>
  );
};


