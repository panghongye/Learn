import React, { useState, useContext } from 'react';
import FlowBuilder, { NodeContext, INode, IRegisterNode, BuilderContext, useDrawer } from 'react-flow-builder';
import { Form, Button, Input, Drawer, Popconfirm, Popover } from 'antd';

import './flow.css';

const ConfigForm: React.FC = () => {
  const { selectedNode: node } = useContext(BuilderContext);
  const { closeDrawer: cancel, saveDrawer: save } = useDrawer();
  const [form] = Form.useForm();
  const handleSubmit = async () => {
    console.log('node', node)
    try {
      const values = await form.validateFields();
      save?.({...values,a:'a'});
    } catch (error) {
      const values = form.getFieldsValue();
      save?.({...values,a:'a'}, !!error);
    }
  };
  return <Form form={form} initialValues={node.data || { name: node.name }}>
    <Form.Item name="name" label="Name" rules={[{ required: true }]}>
      <Input />
    </Form.Item>
    <div>
      <Button onClick={cancel}>取消</Button>
      <Button type="primary" onClick={handleSubmit}>确定</Button>
    </div>
  </Form>
};

const StartNodeDisplay: React.FC = () => {
  const node = useContext(NodeContext);
  return <div className="start-node">{node.name}</div>;
};

const EndNodeDisplay: React.FC = () => {
  const node = useContext(NodeContext);
  return <div className="end-node">{node.name}</div>;
};

const NodeDisplay: React.FC = () => {
  const node = useContext(NodeContext);
  return (
    <div className={`other-node ${node.configuring ? 'node-configuring' : ''} ${node.validateStatusError ? 'node-status-error' : ''}`}>
      {node.data ? node.data.name : node.name}
    </div>
  );
};

const ConditionNodeDisplay: React.FC = () => {
  const node = useContext(NodeContext);
  return (
    <div className={`condition-node ${node.configuring ? 'node-configuring' : ''} ${node.validateStatusError ? 'node-status-error' : ''}`} >
      {node.data ? node.data.name : node.name}
    </div>
  );
};

const registerNodes: IRegisterNode[] = [
  {
    type: 'start',
    name: '开始节点',
    displayComponent: StartNodeDisplay,
    isStart: true,
  },
  {
    type: 'end',
    name: '结束节点',
    displayComponent: EndNodeDisplay,
    isEnd: true,
  },
  {
    type: 'node',
    name: '普通节点',
    displayComponent: NodeDisplay,
    configComponent: ConfigForm,
  },
  {
    type: 'condition',
    name: '条件节点',
    displayComponent: ConditionNodeDisplay,
    configComponent: ConfigForm,
  },
  {
    type: 'branch',
    name: '分支节点',
    conditionNodeType: 'condition',
  },
  {
    type: 'loop',
    name: '循环节点',
    isLoop: true,
    displayComponent: NodeDisplay,
    configComponent: ConfigForm,
  },
];

const defaultNodes = [
  {
    id: 'node-0d9d4733-e48c-41fd-a41f-d93cc4718d97',
    type: 'start',
    name: 'start 0',
    path: ['0'],
  },
  {
    id: 'node-b2ffe834-c7c2-4f29-a370-305adc03c010',
    type: 'branch',
    name: '分支节点 1',
    path: ['1'],
    children: [
      {
        id: 'node-cf9c8f7e-26dd-446c-b3fa-b2406fc7821a',
        type: 'condition',
        name: `条件节点 '1','children', '0' `,
        path: ['1', 'children', '0'],
        children: [
          {
            id: 'node-f227cd08-a503-48b7-babf-b4047fc9dfa5',
            type: 'node',
            name: `普通节点 '1', 'children', '0', 'children', '0'`,
            path: ['1', 'children', '0', 'children', '0'],
          },
        ],
      },
      {
        id: 'node-9d393627-24c0-469f-818a-319d9a678707',
        type: 'condition',
        name: `条件节点 '1', 'children', '1'`,
        children: [],
        path: ['1', 'children', '1'],
      },
    ],
  },
  {
    id: 'node-972401ca-c4db-4268-8780-5607876d8372',
    type: 'node',
    name: '普通节点 2',
    path: ['2'],
  },
  {
    id: 'node-b106675a-5148-4a2e-aa86-8e06abd692d1',
    type: 'end',
    name: 'end 3',
    path: ['3'],
  },
];

const NodeForm = () => {
  const [nodes, setNodes] = useState<INode[]>(defaultNodes);

  const handleChange = (nodes: INode[]) => {
    console.log('nodes change', nodes);
    setNodes(nodes);
  };

  return <FlowBuilder historyTool zoomTool
    onChange={handleChange}
    nodes={nodes}
    registerNodes={registerNodes}
    DrawerComponent={Drawer}
    PopoverComponent={Popover}
    PopconfirmComponent={Popconfirm}
  />
};

export default NodeForm;