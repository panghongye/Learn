import React from "react";
import { useState } from "react";

export default function Antd() {
  const [value, valueSet] = useState("")
  return <div  >
    <input type="text" value={value} onChange={(e) => { valueSet(e.target.value) }} />
    <div>{value}</div>
    <div contentEditable>contentEditable</div>
  </div>
}
