import React, { useState } from 'react';
import { Input, Button, Checkbox, InputNumber, Radio, DatePicker, Select, Upload, FormInstance } from '@ones-design/core';
import { CopyOutlined, DeleteOutlined, DragOutlined } from '@ant-design/icons';
import { Form } from 'antd'
import 'antd/es/form/style/index.css'
import '../C/FormRender.css'
import { DndContext } from '@dnd-kit/core';
import { useDraggable } from '@dnd-kit/core';
import { useDroppable } from '@dnd-kit/core';

export function Droppable(props) {
  const a = useDroppable({
    id: props.id,
  });

  const { isOver, setNodeRef, active, rect, node, over } = a
  return (
    <div ref={setNodeRef} {...props}>
      {props.children}
    </div>
  );
}

export function Draggable(props) {
  const { attributes, listeners, setNodeRef, transform } = useDraggable({
    id: props.id,
  });
  const style = transform ? {
    transform: `translate3d(${transform.x}px, ${transform.y}px, 0)`,
  } : undefined;


  return (
    <div ref={setNodeRef} {...props} style={{ ...props.style, ...style }} {...listeners} {...attributes} >
      {props.children}
    </div>
  );
}


export interface Option {
  [key: string]: any;
  type: 'Input' | 'TextArea' | 'InputNumber' | 'File' | 'Radio' | 'Checkbox' | 'Select:Single' | 'Select:Multiple' | 'DatePicker' | 'Explain' | 'Member' | 'Project' | 'WorkItem' | 'PageGroup' | 'WikiPage'
  name: string
  required?: boolean | undefined
  placeholder?: string
  precision?: number,
  options?: string[]
  props?: any
  value?: any //type =Explain 时的值
  max?: number //type =Member 时 指定最大选择数量
}

export interface IProps {
  pattern: '设计' | '使用'
  options: Option[],
  optionsSet: (options: Option[]) => void
  onchange?: (options: Option[]) => void
  title?: string
  form?: any // Form.useForm()
}

export function FormRender(props: IProps) {
  const { pattern, options, onchange, optionsSet, form } = props
  const [selectedOption, selectedOptionSet] = useState(options[0] || {})
  const [selectedOptionIndex, selectedOptionIndexSet] = useState(0)
  function onValueChange() {
    selectedOptionSet(JSON.parse(JSON.stringify(selectedOption)))
    optionsSet(JSON.parse(JSON.stringify(options)))
    onchange && onchange(JSON.parse(JSON.stringify(options)))
  }

  function onOptionSetChange(k: string, v: any) {
    selectedOption[k] = v
    options[selectedOptionIndex] = selectedOption
    selectedOptionSet(JSON.parse(JSON.stringify(selectedOption)))
    onValueChange()
  }


  function valueAdd(type: string) {
    if (type == 'Input') options.push({ type, name: '单行输入框', required: true, placeholder: '请输入' })
    if (type == 'TextArea') options.push({ type, name: '多行输入框', required: true, placeholder: '请输入' })
    if (type == 'InputNumber') options.push({ type: 'InputNumber', name: '数字输入框', required: true, placeholder: '请输入数字', precision: 0 })
    if (type == 'File') options.push({ type, name: '附件', required: true, })
    if (type == 'Radio') options.push({ type, name: '单选', required: true, options: ['选项1', '选项2'] })
    if (type == 'Checkbox') options.push({ type, name: '多选', required: true, options: ['选项1', '选项2'] })
    if (type == 'Select:Single') options.push({ type, name: '下拉单选', required: true, options: ['选项1', '选项2'] })
    if (type == 'Select:Multiple') options.push({ type, name: '下拉多选', required: true, options: ['选项1', '选项2'] })
    if (type == 'DatePicker') options.push({ type, name: '日期时间', required: true, props: { showTime: true } })
    if (type == 'Explain') options.push({ type, name: '说明文字', required: true, placeholder: '请输入文字说明', value: '说明文字' })
    // ONES
    if (type == 'Member') options.push({ type, name: '成员', required: true, placeholder: '请选择', max: 1 })
    if (type == 'Project') options.push({ type, name: '项目', required: true, })
    if (type == 'WorkItem') options.push({ type, name: '工作项', required: true, })
    if (type == 'PageGroup') options.push({ type, name: '页面组', required: true, })
    if (type == 'WikiPage') options.push({ type, name: 'Wiki页面', required: true, })
    selectedOptionSet(JSON.parse(JSON.stringify(options[options.length - 1] || {})))
    onValueChange()
  }


  return (
    <>
      <DndContext onDragEnd={(a: any) => { valueAdd(a.active.id) }}>
        <div className='form-container'>
          {/* 表单菜单 */}
          {pattern == '设计' && <div className='form-container-menus'>
            <h3>基础控件</h3>
            <div className='form-container-menu'>
              {[
                { label: '单行输入框', type: 'Input' },
                { label: '多行输入框', type: 'TextArea' },
                // { label: '数字输入框', type: 'InputNumber' },
                { label: '附件', type: 'File' },
                { label: '单选', type: 'Radio' },
                { label: '多选', type: 'Checkbox' },
                { label: '下拉单选', type: 'Select:Single' },
                { label: '下拉多选', type: 'Select:Multiple' },
                { label: '日期时间', type: 'DatePicker' },
                { label: '说明文字', type: 'Explain' },
              ].map((a, b) =>

                <Draggable id={a.type} key={a.type} className='form-container-menu-btn' style={{ position: 'relative', zIndex: 1 }}>
                  <Button key={b} className='form-container-menu-btn' onClick={() => valueAdd(a.type)}>{a.label}</Button>
                </Draggable>

              )}
            </div>
            <h3 style={{ marginTop: '20px' }}>ONES控件</h3>
            <div className='form-container-menu'>
              {[
                // { label: '成员', type: 'Member' },
                { label: '项目', type: 'Project' },
                { label: '工作项', type: 'WorkItem' },
                { label: '页面组', type: 'PageGroup' },
                { label: 'wiki页面', type: 'WikiPage' },

              ].map((a, b) => <Button key={b} className='form-container-menu-btn' onClick={() => valueAdd(a.type)}>{a.label}</Button>)}
            </div>
          </div>}
          {/* 表单设计 */}
          <Droppable id={'sss'}>
            <div className='form-container-form-div'>
              <div className='form-container-form'>
                <div className='form-container-form-title'>{props?.title}</div>
                <Form layout='vertical' form={form} name='formControls' className='formControls'>
                  {
                    options.map((item, index) => {
                      return (
                        <div className='FormItemDiv' onClick={(e) => {
                          selectedOptionSet(item)
                          selectedOptionIndexSet(index)
                        }}>
                          <div className='action-btns'>
                            <CopyOutlined className='btn' onClick={() => {
                              let v = JSON.parse(JSON.stringify(item))
                              v.name += '副本'
                              options.splice(index + 1, 0, v);
                              onValueChange()
                            }} />|
                            <DeleteOutlined className='btn' onClick={() => {
                              options.splice(index, 1)
                              onValueChange()
                            }} />
                          </div>
                          <Form.Item className='FormItem' name={item.name} rules={[{ required: item.required }]} label={item.name}>
                            {item.type == 'Input' && <Input placeholder={item.placeholder || '请输入'} crossOrigin='anonymous' />}
                            {item.type == 'TextArea' && <Input.TextArea placeholder={item.placeholder || '请输入'} rows={3} />}
                            {item.type == 'InputNumber' && <InputNumber placeholder={item.placeholder || '请输入'} precision={item.precision} crossOrigin={undefined} />}
                            {item.type == 'File' && < Upload.Dragger />}
                            {item.type == 'Select:Single' && <Select options={item.options?.map((a) => ({ label: a, value: a }))} placeholder={item.placeholder || '请选择'} />}
                            {item.type == 'Select:Multiple' && <Select mode='multiple' options={item.options?.map((a) => ({ label: a, value: a }))} placeholder={item.placeholder || '请选择'} />}
                            {item.type == 'Radio' && <Radio.Group options={item.options} />}
                            {item.type == 'Checkbox' && <Checkbox.Group options={item.options} />}
                            {item.type == 'DatePicker' && <DatePicker {...item.props} placeholder={item.placeholder || '请选择'} />}
                            {item.type == 'Explain' && <Input placeholder={item.placeholder || '请输入'} readOnly value={item.options} crossOrigin={undefined} />}
                            {/* ONES */}
                            {item.type == 'Project' && <Select placeholder={item.placeholder || '请选择'} />}
                            {item.type == 'WorkItem' && <Input placeholder={item.placeholder || '输入工作项标题或 ID 搜索'} crossOrigin={undefined} />}
                            {item.type == 'PageGroup' && <Select placeholder={item.placeholder || '请选择'} />}
                            {item.type == 'WikiPage' && <Select placeholder={item.placeholder || '请选择'} />}
                          </Form.Item>
                        </div>

                      )
                    })
                  }
                  {pattern == '设计' && <div className='formControlsTips'><DragOutlined style={{ marginRight: 10 }} />点击或拖拽左侧控件到此处</div>}
                </Form>
              </div>
            </div>
          </Droppable>
          {/* 属性设置 */}
          {pattern == '设计' && <div className='form-container-setting'>
            <h3>{selectedOption.type}</h3>
            <Form layout='vertical'>
              {(selectedOption.type == 'Input'
                || selectedOption.type == 'TextArea'
                || selectedOption.type == 'InputNumber'
                || selectedOption.type == 'File'
                || selectedOption.type == 'Radio'
                || selectedOption.type == 'Checkbox'
                || selectedOption.type == 'Select:Single'
                || selectedOption.type == 'Select:Multiple'
                || selectedOption.type == 'DatePicker'
                || selectedOption.type == 'Explain'
                || selectedOption.type == 'Member'
                || selectedOption.type == 'Project'
                || selectedOption.type == 'WorkItem'
                || selectedOption.type == 'PageGroup'
                || selectedOption.type == 'WikiPage'
              ) && <Form.Item label='标题' required >
                  <Input value={selectedOption?.name} onChange={(e) => { onOptionSetChange('name', e.target.value); }} placeholder='请输入' crossOrigin={undefined} key={undefined} children={undefined} className={undefined} style={undefined} form={undefined} onSubmit={undefined} autoComplete={undefined} name={undefined} defaultChecked={undefined} defaultValue={undefined} suppressContentEditableWarning={undefined} suppressHydrationWarning={undefined} accessKey={undefined} autoFocus={undefined} contentEditable={undefined} contextMenu={undefined} dir={undefined} draggable={undefined} hidden={undefined} id={undefined} lang={undefined} slot={undefined} spellCheck={undefined} tabIndex={undefined} title={undefined} translate={undefined} radioGroup={undefined} role={undefined} about={undefined} datatype={undefined} inlist={undefined} prefix={undefined} property={undefined} resource={undefined} typeof={undefined} vocab={undefined} autoCapitalize={undefined} autoCorrect={undefined} autoSave={undefined} color={undefined} itemProp={undefined} itemScope={undefined} itemType={undefined} itemID={undefined} itemRef={undefined} results={undefined} security={undefined} unselectable={undefined} inputMode={undefined} is={undefined} aria-activedescendant={undefined} aria-atomic={undefined} aria-autocomplete={undefined} aria-busy={undefined} aria-checked={undefined} aria-colcount={undefined} aria-colindex={undefined} aria-colspan={undefined} aria-controls={undefined} aria-current={undefined} aria-describedby={undefined} aria-details={undefined} aria-disabled={undefined} aria-dropeffect={undefined} aria-errormessage={undefined} aria-expanded={undefined} aria-flowto={undefined} aria-grabbed={undefined} aria-haspopup={undefined} aria-hidden={undefined} aria-invalid={undefined} aria-keyshortcuts={undefined} aria-label={undefined} aria-labelledby={undefined} aria-level={undefined} aria-live={undefined} aria-modal={undefined} aria-multiline={undefined} aria-multiselectable={undefined} aria-orientation={undefined} aria-owns={undefined} aria-placeholder={undefined} aria-posinset={undefined} aria-pressed={undefined} aria-readonly={undefined} aria-relevant={undefined} aria-required={undefined} aria-roledescription={undefined} aria-rowcount={undefined} aria-rowindex={undefined} aria-rowspan={undefined} aria-selected={undefined} aria-setsize={undefined} aria-sort={undefined} aria-valuemax={undefined} aria-valuemin={undefined} aria-valuenow={undefined} aria-valuetext={undefined} dangerouslySetInnerHTML={undefined} onCopy={undefined} onCopyCapture={undefined} onCut={undefined} onCutCapture={undefined} onPaste={undefined} onPasteCapture={undefined} onCompositionEnd={undefined} onCompositionEndCapture={undefined} onCompositionStart={undefined} onCompositionStartCapture={undefined} onCompositionUpdate={undefined} onCompositionUpdateCapture={undefined} onFocus={undefined} onFocusCapture={undefined} onBlur={undefined} onBlurCapture={undefined} onChangeCapture={undefined} onBeforeInput={undefined} onBeforeInputCapture={undefined} onInput={undefined} onInputCapture={undefined} onReset={undefined} onResetCapture={undefined} onSubmitCapture={undefined} onInvalid={undefined} onInvalidCapture={undefined} onLoad={undefined} onLoadCapture={undefined} onError={undefined} onErrorCapture={undefined} onKeyDown={undefined} onKeyDownCapture={undefined} onKeyPress={undefined} onKeyPressCapture={undefined} onKeyUp={undefined} onKeyUpCapture={undefined} onAbort={undefined} onAbortCapture={undefined} onCanPlay={undefined} onCanPlayCapture={undefined} onCanPlayThrough={undefined} onCanPlayThroughCapture={undefined} onDurationChange={undefined} onDurationChangeCapture={undefined} onEmptied={undefined} onEmptiedCapture={undefined} onEncrypted={undefined} onEncryptedCapture={undefined} onEnded={undefined} onEndedCapture={undefined} onLoadedData={undefined} onLoadedDataCapture={undefined} onLoadedMetadata={undefined} onLoadedMetadataCapture={undefined} onLoadStart={undefined} onLoadStartCapture={undefined} onPause={undefined} onPauseCapture={undefined} onPlay={undefined} onPlayCapture={undefined} onPlaying={undefined} onPlayingCapture={undefined} onProgress={undefined} onProgressCapture={undefined} onRateChange={undefined} onRateChangeCapture={undefined} onSeeked={undefined} onSeekedCapture={undefined} onSeeking={undefined} onSeekingCapture={undefined} onStalled={undefined} onStalledCapture={undefined} onSuspend={undefined} onSuspendCapture={undefined} onTimeUpdate={undefined} onTimeUpdateCapture={undefined} onVolumeChange={undefined} onVolumeChangeCapture={undefined} onWaiting={undefined} onWaitingCapture={undefined} onAuxClick={undefined} onAuxClickCapture={undefined} onClick={undefined} onClickCapture={undefined} onContextMenu={undefined} onContextMenuCapture={undefined} onDoubleClick={undefined} onDoubleClickCapture={undefined} onDrag={undefined} onDragCapture={undefined} onDragEnd={undefined} onDragEndCapture={undefined} onDragEnter={undefined} onDragEnterCapture={undefined} onDragExit={undefined} onDragExitCapture={undefined} onDragLeave={undefined} onDragLeaveCapture={undefined} onDragOver={undefined} onDragOverCapture={undefined} onDragStart={undefined} onDragStartCapture={undefined} onDrop={undefined} onDropCapture={undefined} onMouseDown={undefined} onMouseDownCapture={undefined} onMouseEnter={undefined} onMouseLeave={undefined} onMouseMove={undefined} onMouseMoveCapture={undefined} onMouseOut={undefined} onMouseOutCapture={undefined} onMouseOver={undefined} onMouseOverCapture={undefined} onMouseUp={undefined} onMouseUpCapture={undefined} onSelect={undefined} onSelectCapture={undefined} onTouchCancel={undefined} onTouchCancelCapture={undefined} onTouchEnd={undefined} onTouchEndCapture={undefined} onTouchMove={undefined} onTouchMoveCapture={undefined} onTouchStart={undefined} onTouchStartCapture={undefined} onPointerDown={undefined} onPointerDownCapture={undefined} onPointerMove={undefined} onPointerMoveCapture={undefined} onPointerUp={undefined} onPointerUpCapture={undefined} onPointerCancel={undefined} onPointerCancelCapture={undefined} onPointerEnter={undefined} onPointerLeave={undefined} onPointerOver={undefined} onPointerOverCapture={undefined} onPointerOut={undefined} onPointerOutCapture={undefined} onGotPointerCapture={undefined} onGotPointerCaptureCapture={undefined} onLostPointerCapture={undefined} onLostPointerCaptureCapture={undefined} onScroll={undefined} onScrollCapture={undefined} onWheel={undefined} onWheelCapture={undefined} onAnimationStart={undefined} onAnimationStartCapture={undefined} onAnimationEnd={undefined} onAnimationEndCapture={undefined} onAnimationIteration={undefined} onAnimationIterationCapture={undefined} onTransitionEnd={undefined} onTransitionEndCapture={undefined} pattern={undefined} type={undefined} required={undefined} max={undefined} min={undefined} maxLength={undefined} disabled={undefined} allowClear={undefined} bordered={undefined} readOnly={undefined} multiple={undefined} accept={undefined} alt={undefined} capture={undefined} checked={undefined} formAction={undefined} formEncType={undefined} formMethod={undefined} formNoValidate={undefined} formTarget={undefined} height={undefined} list={undefined} minLength={undefined} src={undefined} step={undefined} width={undefined} onPointerEnterCapture={undefined} onPointerLeaveCapture={undefined} suffix={undefined} enterKeyHint={undefined} htmlSize={undefined} addonBefore={undefined} addonAfter={undefined} />
                </Form.Item>}

              {(selectedOption.type == 'Input'
                || selectedOption.type == 'TextArea'
                || selectedOption.type == 'InputNumber'
                || selectedOption.type == 'Radio'
                || selectedOption.type == 'Checkbox'
                || selectedOption.type == 'Select:Single'
                || selectedOption.type == 'Select:Multiple'
                || selectedOption.type == 'DatePicker'
                || selectedOption.type == 'Member'
                || selectedOption.type == 'Project'
                || selectedOption.type == 'WorkItem'
                || selectedOption.type == 'PageGroup'
                || selectedOption.type == 'WikiPage'
              ) && <Form.Item label='默认提示'  >
                  <Input value={selectedOption.placeholder} onChange={(e) => { onOptionSetChange('placeholder', e.target.value); }} placeholder='请输入' crossOrigin={undefined} />
                </Form.Item>}

              {(selectedOption.type == 'InputNumber') && <Form.Item label='小数位数' required>
                <InputNumber precision={0} onChange={(e) => { onOptionSetChange('precision', e); }} min={0} placeholder='不限制' crossOrigin={undefined} />
              </Form.Item>}

              {(selectedOption.type == 'Radio' || selectedOption.type == 'Checkbox' || selectedOption.type == 'Select:Single' || selectedOption.type == 'Select:Multiple') && <Form.Item label='选项' required >
                {selectedOption.options?.map((a, b: any) => {
                  return <Form.Item key={b}> <Input value={a} placeholder='请输入' style={{ width: '70%' }} onChange={(e) => {
                    let o = selectedOption?.options || [];
                    o[b] = e.target.value;
                    options[selectedOptionIndex] = selectedOption;
                    onValueChange();
                  }} crossOrigin={undefined} /> <DeleteOutlined onClick={() => {
                    selectedOption?.options?.splice(b, 1)
                    onValueChange()
                  }} /> </Form.Item>
                })}
                {selectedOption?.options?.length && selectedOption?.options?.length < 10 &&
                  <Button type='link' style={{ display: 'block' }} onClick={() => {
                    let o = selectedOption.options || []
                    o.push('')
                    onValueChange()
                  }}>+新增选项</Button>
                }
              </Form.Item>}

              {(selectedOption.type == 'Input'
                || selectedOption.type == 'TextArea'
                || selectedOption.type == 'InputNumber'
                || selectedOption.type == 'File'
                || selectedOption.type == 'Radio'
                || selectedOption.type == 'Checkbox'
                || selectedOption.type == 'Select:Single'
                || selectedOption.type == 'Select:Multiple'
                || selectedOption.type == 'DatePicker'
                || selectedOption.type == 'Member'
                || selectedOption.type == 'Project'
                || selectedOption.type == 'WorkItem'
                || selectedOption.type == 'PageGroup'
                || selectedOption.type == 'WikiPage'
              ) && <Form.Item label='其他' >
                  <Checkbox checked={selectedOption.required} onChange={(e) => { onOptionSetChange('required', e.target.checked) }} /> 必填
                </Form.Item>}

              {(selectedOption.type == 'DatePicker') && <Form.Item required label='日期格式' >
                <Select options={[{ label: '年-月-日', options: false as any }, { label: '年-月-日 时:分:秒', options: true as any }]} placeholder='请选择'
                  value={selectedOption.props.showTime} onChange={(e) => { onOptionSetChange('props', { ...selectedOption.props, showTime: e }) }} />
              </Form.Item>}

              {(selectedOption.type == 'Explain') && <Form.Item required label='说明文字' >
                <Input.TextArea value={selectedOption.options} maxLength={1000} placeholder='请输入说明文字' rows={3} onChange={(e) => { onOptionSetChange('options', e.target.value) }} />
              </Form.Item>}


              {(selectedOption.type == 'Member') && <Form.Item label='选择方式' >
                <Radio.Group layout='vertical' value={selectedOption.max && selectedOption.max > 1 ? '多选' : '单选'}>
                  <Radio value='单选' onClick={() => { onOptionSetChange('max', 1) }}>单选</Radio>
                  <Radio value='多选'  >多选，最大可选择数量<InputNumber value={selectedOption.max} min={1} precision={0} style={{ marginLeft: '10px', width: '100px' }} onChange={(e) => {
                    onOptionSetChange('max', e)
                  }} crossOrigin={undefined} /></Radio>
                </Radio.Group>
              </Form.Item>}

            </Form>
          </div>}
        </div >

      </DndContext>
    </>
  );
};


export default () => {
  const [options, optionssSet] = useState([
    {
      type: 'Input',
      name: '单行输入框',
      placeholder: 'placeholder',
      required: true,
    },
    {
      type: 'TextArea',
      name: '多行输入框',
      placeholder: 'placeholder',
      required: true,
    },
  ]) as any
  return <FormRender pattern='设计' options={options} optionsSet={optionssSet} />
}