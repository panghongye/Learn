import store from '../store/zustand'

export default function Z() {
  const { count, increment, decrement } = store();

  const A = () => {
    const  st = store();
    return <p>{st.count}</p>
  }

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
      <A />
    </div>
  );
}