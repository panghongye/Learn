import React, { useState } from 'react';

const DragAndDropExample = () => {
  const [droppedItem, setDroppedItem] = useState(null);

  const handleDragStart = (event) => {
    event.dataTransfer.setData('text/plain', JSON.stringify({ a: 1 }));
  };

  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const data = event.dataTransfer.getData('text/plain');
    console.log('ddd', JSON.parse(data))
    setDroppedItem(data);
  };

  return (
    <div>
      <div
        id="draggable"
        draggable="true"
        onDragStart={handleDragStart}
        style={{ width: '100px', height: '100px', backgroundColor: 'yellow' }}
      >
        拖动我
      </div>

      <div
        id="droppable"
        onDragOver={handleDragOver}
        onDrop={handleDrop}
        style={{ width: '200px', height: '200px', backgroundColor: 'blue', color: 'white', textAlign: 'center', lineHeight: '200px' }}
      >
        {droppedItem ? `已拖放: ${droppedItem}` : '拖放到这里'}
      </div>
    </div>
  );
};

export default DragAndDropExample;
